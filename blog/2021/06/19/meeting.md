[[!meta title="Iteration planning: June 19"]]
[[!tag meeting]]
[[!meta date="2021-06-19 12:00 +0000"]]

[[!toc levels=2]]

# Review of actions from last meeting

* There were no actions. The action about [[!issue 22]] was a mistake.

# Review of the iteration that has ended

[[!milestone 30]] was for this iteration. We closed all issues picked
for the iteration:

* [[!issue 194]]&mdash;trial release
* [[!issue 195]]&mdash;`./check` to tail logs on failure
* [[!issue 197]]&mdash;`lib/daemon.py` to break long lines in log
* [[!issue 199]]&mdash;honour `JAVA_HOME` for Pandoc
* [[!issue 200]]&mdash;sort lists in metadata output
* [[!issue 201]]&mdash;move `bindings-ubm` into `tests`
* [[!issue 203]]&mdash;document `build.rs`

All issues tagged `current-goal` are now closed, except [[!issue
125]], which is the tracker issue for getting people to try Subplot.

# Review of the repositories

We reviewed the `subplot-web` and `subplot-container-images`
repositories, and there were no open issues, no open merge requests,
no extra branches, and no unsuccessful CI pipelines. The `subplot`
repository had no open merge requests, no unnecessary branches, and
the pipeline for the `main` branch was successful. There is one
work-in-progress branch for changing the Subplot subplot to use Rust
instead of Python for step implementations. All good: no pending
cleanup work from this iteration.

# Goal for this iteration

We discussed the goal for this iteration, which is to get a few people
actually try Subplot. We reformulated [[!issue 125]] accordingly. We
think all code changes are ready for the first outside users, so the
work we want to do is to make a release for them to try, and then be
ready to react to any feedback quickly.

# Issue review

We reviewed all issues this time. Most issues are not relevant to
near-term goals, but to manage that better, we created a label for
"goal two", after the current goal ("goal one"). Goal one is about
getting others to try Subplot; goal two is to make Subplot nicer to
use. Once goal one is done, we'll rename the `current-goal` tag to
`goal:1`, and the `goal:2` tag to `current-goal`.

We added the following issues to the `goal:2` tag:

- [[!issue 20]]&mdash;conditional scenarios
- [[!issue 22]]&mdash;location of helper binaries
- [[!issue 74]]&mdash;environment variables for Bash
- [[!issue 120]]&mdash;list all matching bindings
- [[!issue 190]]&mdash;test suite is slow
- [[!issue 188]]&mdash;libraries using regex patterns
- [[!issue 196]]&mdash;Python runner stops on first failure
- [[!issue 198]]&mdash;unified Subplot libraries

We decided to pick one difficult issue for each goal. For goal two
that is [[!issue 20]].

We closed [[!issue 154]]] as hopefully done, and [[!issue 191]] as
unwanted.

After the review there were 33 open issues.

# Current goal

Our current goal is to get Subplot the software and project into such
a shape that we feel confident that we can ask a few people to give it
a proper try. This requires for Subplot to have releases that are easy
to install, tolerably easy to use, and to have at least some
rudimentary documentation aimed at beginners.

# Plan for next iteration

We created [[!milestone 31]], with two issues:

- [[!issue 125]]&mdash;get people to try Subplot
- [[!issue 204]]&mdash;create release

# Other business

We discussed using GitLab issues for iteration meeting agenda. For
now, we'll stick to what we have, since it works for the two of us,
but as soon as others want to join, we'll re-evaluate. Using issues,
or possibly merge requests, might be useful if contributors aren't in
time zones close to the European one.

We discussed using `cargo release` to simplify the release process,
but decided to evaluate it in more detail later.

We discussed release cadence, and decided to start each iteration by
making a release, unless there is something to prevent a release.
Making the release at the start of an iteration means it's less
hurried, and so more likely to be correct. We also decided to automate
the release process as we go along.

# Actions

No actions.
