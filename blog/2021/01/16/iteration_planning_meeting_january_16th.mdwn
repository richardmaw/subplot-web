[[!meta title=" Iteration planning meeting: January 16th"]]
[[!tag meeting]]
[[!meta date="2021-01-16 13:00"]]

# Actions from last time

These haven't been done:

- Lars or Daniel to schedule pairing session with Dan to help him work
  through [[!issue 22]]
  - Daniel failed to even attempt to arrange something, though Dan was likely
    too busy/hurty so we'll carry this forward..

These have been done:

- Lars and Daniel to set up a meeting to discuss i18n and l10n (done)
  - We had a discussion and decided tentatively to go with Fluent unless
    we find a good reason to try something simpler first.

# The iteration that is ending

Lars didn't do much for the iteration specifically, but did finish [[!mr 119]]
and reviewed MRs from Daniel. Lars was very busy with Obnam and driving the
train at work.

We finished [[!milestone 20]], having fixed [[!issue 144]], [[!issue 143]].

Progress was made on [[!issue 131]].

We closed [[!milestone 20]] and created [[!milestone 21]].

# Pending MRs

There were no pending MRs on Subplot-Web, There were no pending MRs on Subplot,
after Lars had a last-minute review spree an hour before the meeting.

# Branch review

No open branches on subplot-web, nor on subplot itself.

# Issue review

Subplot-web has no issues. Subplot itself has 36 open issues.

Only 2 were touched since the last iteration.

- [[!issue 148]] was opened by Daniel. We had a good discussion, logged it
  in the issue, and tentatively put the issue into the new iteration
- [[!issue 131]] we are happy with the discussion we had in the iteration,
  and the labels etc. we placed on the issue. We are not prepared to act on it
  in this coming iteration.

Regarding issues previously queued

- [[!issue 147]] tentatively moved to the new iteration
- [[!issue 146]] tentatively moved to the new iteration
- [[!issue 136]] tentatively moved to the new iteration
- [[!issue 118]] removed from the iteration cycle for now
- [[!issue 108]] tentatively moved to the new iteration
- [[!issue 22]] carried to the new iteration as discussed above
- [[!issue 116]] removed from the iteration cycle for now

# Planning the next iteration

We are happy with the set of things in the iteration, having done some time
estimation we believe we can get it all done.

# Regarding the tracking of getting new users to Subplot

Daniel had a brief rant about documenting error codes and providing in-system
help where we can. This is a long-term goal and not something to prevent new
users getting into Subplot but it will drive adoption eventually.

We're happy that we know what we need to do to move forward, but we're also
adamant that we don't want to do it this iteration.

# Regarding Subplot in Debian

We may have to delier vegan cake in NYC to make it happen, but we won't do anything
until Bullseye is either very close to, or actually, releasing.

# AoB

Lars wanted to talk about VMDB2 and concurrent scenarios…

We had a discussion about how this could be dealt with and we decided that it
may be easiest to wrapper a `test.py` with a `Makefile` for now, committing the
outputs of `sp-codegen` and `sp-docgen` to the vmdb2 repository for now.

# Actions

- Lars or Daniel to schedule pairing session with Dan to help him work
  through [[!issue 22]]
