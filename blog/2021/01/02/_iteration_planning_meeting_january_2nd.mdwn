[[!meta title=" Iteration planning meeting: January 2nd"]]
[[!tag meeting]]
[[!meta date="2021-01-02 15:24"]]


# Actions from last time

These haven't been done:

- Daniel and Lars to set up a call about [[!issue 131]].
  - not done
- Schedule pairing session with Dan to help him work through [[!issue 22]]
  - not done due to life happening

These have been done:

- Daniel to extract `cargo fmt` commit from [[!mr 105]] into a new MR for merging.
- Daniel to split typemap work out into a separate MR so that it can be reviewed
  and merged independently of the Rust runner work. (Daniel to file an issue
  before doing this to include our decided approach).
- Lars to file MR to add `.tera` to bash and python template
- Daniel to file an MR for splitting out the filters in the Rust MR.
- Daniel to file an issue about embedding templates with a view to `cargo install`
  related to [[!issue 66]] and [[!issue 109]].
  - filed as [[!issue 134]]
- Lars to open an issue about how we don't have the feature table for runners.
  - filed as [[!issue 133]]

# The iteration that is ending

We extended the deadline because life events got in the way, skipping
the meeting that was meant two happen two weeks ago.

Daniel got the Rust template done, which was a significant chunk of
work. It's a good start, if maybe not mature yet. Maturity will come
when we use it for real.

Daniel is sad that he had tot make a copy of the runcmd.md file just
so that he could change the YAML metadata. This may indicate there's
need for more flexibility in Subplot. We haven't filed an issue about
this yet, as it'd be too woolly.

We finished [[!milestone 19]], having fixed [[!issue 130]], [[!issue
129]], [[!issue 123]], [[!issue 101].

Progress was made on [[!issue 131]], [[!issue 108]], [[!issue 22]].

We closed [[!milestone 19]] and created [[!milestone 20]].

# Pending MRs

[[!mr 119]] is still open. It's a leftover from [[!milestone 19]].
Daniel had left a comment, Lars will respond off-line.

There were no pending merge requests in subplot-web.

# Branch review

No open branches on subplot-web, and subplot itself had one that was
created by mistake, and was deleted during the meeting.

# Issue review

Subplot-web has no issues. Subplot itself has 38 issues.

- [[!issue 16]]: still blocked, Daniel is starting to look at blocker
- [[!issue 27]]: not yet
- [[!issue 36]]: not yet
- [[!issue 82]]: not yet
- [[!issue 97]]: adjusted labels, not yet
- [[!issue 23]]: not yet
- [[!issue 94]]: not yet
- [[!issue 77]]: not yet
- [[!issue 34]]: adjusted labels, not yet
- [[!issue 98]]: not yet
- [[!issue 78]]: not yet
- [[!issue 126]]: adjusted labels
- [[!issue 127]]: not yet
- [[!issue 20]]: tracking issue, not yet
- [[!issue 120]]: adjusted labels, not yet
- [[!issue 96]]: not yet
- [[!issue 74]]: noted that done for python, not bash yet, or rust
- [[!issue 6]]: not yet
- [[!issue 118]]: TBD
- [[!issue 119]]: TBD
- [[!issue 125]]: TBD
- [[!issue 133]]: adjusted labels, not yet
- [[!issue 134]]: not yet
- [[!issue 137]]: not yet
- [[!issue 138]]: updated description
- [[!issue 141]]: not yet
- [[!issue 143]]: added note
- [[!issue 144]]: assigned to Daniel
- [[!issue 145]]: closed as duplicate
- [[!issue 136]]: we discussed and commented on issue
- [[!issue 146]]: assigned to Lars
- [[!issue 147]]: we discussed; assign to Lars
- [[!issue 131]]: arrange meeting
- [[!issue 108]]: assign to Lars
- [[!issue 22]]: assign to Dan
- [[!issue 116]]: tentatively in iteration


# Planning the next iteration

We then went through our tentative plan for issues in [[!milestone
20]]. We ended up with 9 issues assigned, and on stretch goal.

[[!issue 22]] is assigned to Dan, in the hope he is able to work on
it. If not, that's OK.

# Regarding the tracking of getting new users to Subplot

We need documentation more than anything else. We decided that we have
a rudimentary tutorial, and implementation documentation in the form
of a subplot, and we need a reference manual and a "how to" manual to
goes deeper into how to use Subplot that the tutorial. Not all of
these are needed to getting new people to try Subplot.

How much of a reference manual will we need, initially? We think
details of how to implement a step functions is paramount, and that
the tutorial should be enough to get started. A reference manual is
probably not necessary, for now. Formal specification of input
languages not necessarily massively useful for others, and we
ourselves get that in the Subplot subplot.

# Regarding Subplot in Debian

We discussed this and agreed that since Debian is about to enter a
release for the release of Debian 11, it would probably be polite to
let Debian people concentrate on getting that out before asking anyone
to spend effort on packaging Subplot for Debian.

# Actions

- Lars and Daniel to set up a meeting to discuss i18n and l10n (done)
- Lars or Daniel to schedule pairing session with Dan to help him work
  through [[!issue 22]]
