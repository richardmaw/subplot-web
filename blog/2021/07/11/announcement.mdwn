[[!meta title="Subplot version 0.2.1 release - please try it"]]
[[!tag announcement]]
[[!meta date="2021-07-03 13:40"]]

Capture and communicate acceptance criteria for software and systems,
and how they are verified, in a way that’s understood by all project
stakeholders, including end-users.

Subplot is a set of tools for specifying, documenting, and
implementing automated acceptance tests for systems and software.
Subplot tools help produce a human-readable document of acceptance
criteria and a program that automatically tests a system against those
criteria.

This is the first release meant for use by others than the Subplot
developers. We hope that at least a few brave people will try it out
and let us know what they think, even if Subplot is at this stage
considered alpha quality software.

* [Home page](https://subplot.liw.fi/)
* [Installation](https://subplot.liw.fi/download/)
* [Really simple tutorial](https://subplot.liw.fi/tutorial/)
* [More advanced tutorial](https://doc.subplot.liw.fi/website.html)
* [Git](https://gitlab.com/subplot/subplot)

Your friendly Subplot developers, Lars and Daniel.
