[[!meta title="Iteration planning: February 27th"]]
[[!tag meeting]]
[[!meta date="2021-02-27 15:30"]]

[[!toc levels=2]]

# Review of actions from last meeting.

- Dan remains not in a position to work on [[!issue 22]] which is fine, it can
  wait until he is able to help. We're carrying this action

- Daniel to file an issue to create issue templates based on
  <https://third-bit.com/2021/01/18/how-to-write-a-memo/>. We're
  carying this action.


# Review of the iteration that has ended

We've been busy with life and work and other projects this iteration,
so we didn't get everything done for [[!milestone 23]]. There's
lib/files work, and command line interface refactoring that may be
carried over.

We did achieve:

- [[!issue 153]] - Subplot's source tree has a lot of subplots now -
  should that be cleaned up?
- [[!issue 157]] - Implement directory handling in lib/files for
  Python
- [[!issue 159]] - Implement directory handling in lib/files for Bash
  - there doesn't seem to be a lib/files for Bash; issue open to be
    discussed in this meeting
  - closing since there are no Bash libraries yet
- [[!issue 108]] - tried again to test an older Subplot with the
  current Subplot, and found a new blocker
  - issue still open; we'll discuss it in this meeting
  - we'll wait until we have a single subplot binary, issue 164, which
    will have the resources built in
 - [[!issue 167]] - Rust lib/files "directory exists" step functions
   don't check the path is a directory

# Review of the repositories

There were no pending merge requests for subplot-web. There is a WIP MR
for some work Daniel is doing in subplot.

There were no extra branches in subplot-web.

# Issue review

We created the `current-goal` issue label to mark issues relevant to
the current goal.

We reviewed all issues and updated them based on discussion.


# Current goal

Our current goal is to get Subplot the software and project into such
a shape that we feel confident that we can ask a few people to give it
a proper try. This requires for Subplot to have releases that are easy
to install, tolerably easy to use, and to have at least some
rudimentary documentation aimed at beginners.

# Plan for next iteration

We created [[!milestone 24]] for the iteration starting now and ending
in two weeks on 2021-03-13.

We picked the following issues to work on for Lars:

- [[!issue 166]] - daemon.py busy-waits for port to be open
- [[!issue 162]] - Change ./check to treat Rust warnings as errors,
  with an option to override that

For Daniel:

- [[!issue 168]] - Running ./check modifies subplotlib/tests/files.rs
- [[!issue 164]] - Refactor CLI into single subcommand binary
- [[!issue 161]] - Use git-testament for versioning
- [[!issue 116]] - Subplot documentation needs to explain scenario
  language more effectively

Up for grabs if anyone has time:

- [[!issue 22]] - PlantUML jar and java locations should be
  configurable
  - depends on Dan's availability.

# What do we need to persuade others?

Nothing new came up.

# Actions

- Lars or Daniel to schedule pairing session with Dan to help him work
  through [[!issue 22]].

- Daniel to file an issue to create issue templates based on
  <https://third-bit.com/2021/01/18/how-to-write-a-memo/>.
