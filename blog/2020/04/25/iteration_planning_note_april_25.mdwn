[[!meta title="Iteration planning note: April 25"]]
[[!tag meeting]]
[[!meta date="2020-04-25 20:11"]]

# What has happened

* We got everything done, bar finalizing the merge request to add
  [shell as an output
  language](https://gitlab.com/subplot/subplot/-/issues/4) for
  codegen. That one lacks a small modification to be done.
  - [Avoid Pandoc::add_filter](https://gitlab.com/subplot/subplot/-/issues/43)
  - [Do lints in one
    place](https://gitlab.com/subplot/subplot/-/issues/42)
  - [Bug fix: first scenario step uses
    alias](https://gitlab.com/subplot/subplot/-/issues/39)
  - [Simple
    patterns](https://gitlab.com/subplot/subplot/-/issues/33)
  - [Check embedded files have unique
    names](https://gitlab.com/subplot/subplot/-/issues/4)

# Discussion

* We reviewed and updated all open merge requests and issues, and
  picked issues to work on for the next iteration. We picked five
  issues to work on, two to discuss, and to as a stretch goal if
  there's extra time.

* We discussed whether we should require Subplot to be buildable with
  the Rust toolchain in Debian 10 (buster). The changes to support
  shell trigger a panic in that rustc (the same upstream version
  doesn't). We decided that since Subplot will not go into buster, and
  we don't use that rustc for our own work, we don't need to be
  buildable with that rustc.

  We'd like the rustc in the next Debian stable version to work to
  build Subplot, though.

* We discussed using the "converntional commits" specification for
  Subplot. We decided that since Subplot is such a small project, it's
  not useful enough at this time. We may reconsider later.

* Reviewing all open issues in these meetings is useful, but also time
  consuming. We agreed to only do it every other time from now on.

# Actions

* Lars to make the remaining changes to the shell support merge
  request. Daniel to re-review.

* Lars to set his personal CI to use rustup for installing Rust,
  instead of buster's rustc.

* Lars to provide Daniel with a reproducible test case, crash output,
  and minimum commit range to reproduce the rustc crash. Daniel will
  have a look if he can see what causes the crash.
