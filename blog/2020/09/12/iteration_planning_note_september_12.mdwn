[[!meta title="Iteration planning note: September 12th"]]
[[!tag meeting]]
[[!meta date="2020-09-12 17:30"]]

# What has happened?

We finished [[!milestone 13]]. Many small tasks were done, and while that looks
like many items of work, it wasn't a huge amount over-all due to the size of
them. Daniel did not get his [[!issue 20]] done, and the Python context
namespace issue ([[!issue 80]]) was also not done.

We chose therefore to demilestone [[!issue 20]].

# Review

We had not MRs or branches to review. We reviewed issues younger than 2 weeks
from last change, picking a few for the new iteration.

We reviewed Lars' questions on the [[!issue 53]] and decided:

- Code/Test in the same file is OK until the tests overwhelm the code
- We do not have complex enough code to warrant multiple crates at this time
  as sibling modules are sufficiently protected from one another.
- We want to split out a `src/doc.rs` module for the `Document` struct and
  friends. [[!issue 88]] was created for this.

We went through [[!issue 82]] and wrote a rough plan of action and a set of
labels to indicate this is not hard and we'd love help to get it done.

We added a brief explanation more to [[!issue 74]].

The new iteration [[!milestone 14]] has TODO assigned issues:

- [[!issue 80]] (moved from 13) assigned to Lars
- [[!issue 86]] we logged some suggests here too, assigned to Lars
- [[!issue 88]] assigned to Lars
- [[!issue 85]] we added some suggestions for other `:foo` too
- [[!issue 83]]

# Discussion

Lars would like to review the codebase with a view to deciding what is the next
step for [[!issue 53]] but as of now there's not really anything good to format
the content well enough for him to be comfortable reviewing it on his
reMarkable.

Regarding the use of subplot by others, we previously said that we needed the
documentation to cover everything that Subplot lets you do. Lars is going to
talk with a friend/colleague of his who is a sysadmin with a background in
technical writing so if she's interested then it's possible she might be able to
help with that.

With respect to having `subplot.liw.fi`'s content on gitlab, the only benefit
Daniel can think of is that it'd be easier for third parties to contribute
changes. In the end we decided that until someone says "I would contribute if
only the website were on Gitlab" we'll stick with things as they are.

We discussed possible issues which a newcomer to Rust and to the project might
find easy to do. [[!issue 22]] (The plantuml one) might be a good one to
start with.

We should also keep our eyes peeled for "easy" Rust changes which we could file
easy/mentor/help-wanted issues to file.

# Actions

No unusual actions came out of the meeting.
