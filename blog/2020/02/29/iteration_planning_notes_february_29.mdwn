[[!meta title="Iteration planning notes: February 29"]]
[[!tag meeting]]
[[!meta date="2020-02-29 17:37"]]

# What has happened

* Lars added `--date` option; it is merged.

* Lars started working adding a "base directory" so that the policy
  decision for resolvine filenames for bindings and filenames is
  brough up to the main function in the various programs, rather than
  being an implicit current working directory and handled deep in the
  call stack.

  However, to do this it turned out that some changes to the
  rust-pandoc crate were needed. Daniel has taken those on.

* Lars added Debian package runtime dependencies and suggestions for
  Pandoc and other tools that are necessary, and LaTeX for PDF
  generation.

# Discussion

* There have been no volunteers for alpha testers. We will continue
  developing Subplot and use ourselves as alpha testers. Lars is
  having some success introducing Subplot at his work.

* We discussed GitLab labels for issues. They have been added now.

* We chose issues for the next iteration. A milestone in GitLab has
  been created, and issues assigned to it.

* We discussed the approach for scalability testing, and agreed on an
  approach.

* We agreed that we will be doing merges for each other in the future,
  rather than self-merging.

# Actions

* Lars to add issue labels.

* Lars to work on the issues chosen for this iteration, and Daniel to
  review and merge.
