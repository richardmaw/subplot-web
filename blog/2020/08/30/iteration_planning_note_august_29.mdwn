[[!meta title="Iteration planning note: August 29"]]
[[!tag meeting]]
[[!meta date="2020-08-30 12:34"]]

# What has happened?

We extended [[!milestone 12]] due to life events. We made some
progress, but didn't finish either of the two tasks that were assigned
to the iteration.

# Review

We had not MRs or branches to review. We reviewed all issues, picking
a few for the new iteration.

The new iteration [[!milestone 13]] has three assigned issues:

* Daniel: [[!issue 20]]
* Lars: [[!issue 65]]
* Lars: [[!issue 70]]
* Lars: [[!issue 81]]

There's a few extra ones that should be very quick, in case we have
time.

# Discussion

We discussed briefly what we need before we can start inviting others
to try Subplot for real. At this point, we can think of two things:
better documentation for Subplot itself, aimed at stakeholders in
projects using Subplot, and better testing of Subplot against
accidental breakage.

For the testing, we'll set up a set of chosen projects that use
Subplot, and run their subplots at least once per Subplot iteration.
The goal is to make breaking changes consciously, not accidentally.

We also discussed making merge reviews quicker. The consensus is that
we should ping each other repeatedly if there's reviews.

# Actions

Lars to write these meeting minutes.
