[[!meta title="Iteration planning note: June 6"]]
[[!tag meeting]]
[[!meta date="2020-06-06 21:04"]]

# What has happened?

* We got almost everything done. One issue has an MR in process.
  Another is not even started, and is carried over.
  
# Discussion

* We've both been quite busy with other things, such as life. The
  pandemic and unrest in the US aren't helping.

* There's **one outstanding merge request**.
  
* We reviewed **recently changed issues**. Nothing seems terribly
  urgent.

* We planned the next iteration with a lighter load, since we expect
  to again be busy with other things.
  
# Actions

* Lars to write these notes.
