[[!meta title="Iteration planning notes"]]
[[!tag meeting]]
[[!meta date="2019-10-22 11:17"]]

Daniel and Lars met online to discuss recent happenings and to plan the
next iteration. There was a long pause due to technical difficulties
(read: the Internet was down and it took a long time for the ISP to
fix it).

# What has happened

* Lars gave a talk about Subplot to the Rust Finland meetup.

* Development is slowly making progress. Docgen is making output,
  though typesetting is not yet informed by step bindings. That will
  happen next. This will also be useful for codegen.

* Subplot is now (also) hosted on gitlab.com.
  <https://gitlab.com/subplot/subplot>
  (repo moved 2021-04-29)

# Discussion

* We'll use GitLab for issue tracking, for now.

* We'll wait until Subplot at least does typesetting informed by
  bindings before approaching friends and colleagues and contacts for
  more feedback.

* We'll start by only implementing regular expressions for patterns,
  even though we'll want something else in the long run. We know, from
  prototype trial, that many people, including programmers, find
  regexes painful to use. Also, later on we'll want to have typed
  captures from steps&mdash;string, integer, filename, JSON,
  etc&mdash;for better compile-time error checking, and easier
  function implementation. Simple first, powerful later.

* We'd love to have something like plantuml for code blocks, but
  preferably something better. plantuml is written in Java, and the
  Debian packaging of some of its dependencies are a bit tricky to
  install in a container. It's also a little slow. But we don't know
  of anything better. We might look into this later, but it's not an
  urgent concern. We can live with just dot, for now.

* We don't really want to call Subplot a BDD tool, since it's testing
  is so much less important for Subplot than the communication of
  acceptance criteria, and building a consensus on them and their
  verification.

* A long discussion on Gherkin, Cucumber, and whether we should base
  Subplot on one or both of them. We decided against that. It seems to
  us that Gherkin is more about expressing the tests, and less about
  documenting the consensus of what acceptance criteria are, and
  Subplot emphasises the consensus heavily. We also want the freedom to
  improve the input language for Subplot without rupturing the
  Cucumber ecosystem with incompatible dialects. The Subplot language
  is inspired and influenced by Gherkin, but clearly a separate thing,
  despite the similarities.

  That said, we intend to borrow from Gherkin anything we like. We
  already decided to borrow "but" as an alias for "and", and will
  look into parameterised scenarios and having tables of parameters
  to allow running the same steps in a variety of ways.

* We decided to make the first output language codegen support so to
  be shell. It is the easiest for our potential users to get started
  with, and sufficient for many things. Later, we'll add support for
  Python and Rust.

# Planned actions for new iteration, to end November 2

Daniel:

* Set up a shared way to keep track of things for the next meeting's
  agenda.

* Mentor Lars on Rust. Give feedback on bindings support as it's
  developed.

Lars:

* Add note to the "contributing" page on the website to ask for help
  with site content and structure.

* Add a list of talks about Subplot to the site.

* Add link to site to GitLab repository. Also, README.

* Put a copy of the success levels for Subplot on the site.

* Develop a first rough version of bindings support for docgen. Does
  not need to support codegen yet.
