#!/bin/sh

set -eu

libdir="$1"
destdir="$2"

find "$destdir" -mindepth 1 -delete
sed -i "s,^destdir:.*,destdir: $destdir," ikiwiki.setup
ikiwiki --setup ikiwiki.setup --rebuild --verbose --libdir "$libdir"

# Pandoc runs the filter in srcdir. Filter creates images in
# ./foo-images directories, under srcdir. Ikiwiki doesn't know about
# those files, since they get created while it is running. This copies
# image directories to destdir so they show up when the rendered site
# is viewed.
#
# NOTE: This needs to list the image directories when more image types
# are added.
for dir in dot-images roadmap-images
do
    if [ -e "$dir" ]
    then
       cp -a "$dir" "$destdir/."
    fi
done
