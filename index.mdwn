[[!meta title="Subplot"]]

[[Download]] &mdash;
[[Documentation|docs]] &mdash;
[[Contact]] &mdash;
[[License]] &mdash;
[[Blog]]

[[!img subplot.svg class=float_right
   alt="Subplot alpha logo"]]


## When all stakeholders really need to understand acceptance criteria

Capture and communicate acceptance criteria for software and systems,
and how they are verified, in a way that's understood by all project
stakeholders, including end-users.

Subplot is a set of tools for specifying, documenting, and
implementing automated acceptance tests for systems and software.
Subplot tools help produce a human-readable document of acceptance
criteria and a program that automatically tests a system against those
criteria.

Subplot is currently in its ALPHA stage.


## Latest from the project

<div class="newslist">
[[!inline pages="page(blog/*)" 
  limit=5 template=titlepage archive=yes trail=no feeds=no]]
</div>


## Participate

Subplot is a community project: it is developed by some people who
want to do it. You could help!

* [[Governance]] model
* [[Code of conduct|conduct]]
* [[Architecture]]
* [[Definition of success|success]]
* [[Contributing]] to the project
* [[Contact]] the project

Please note that this project is released with a [[Contributor Code of
Conduct|conduct]]. By participating in this project you agree
to abide by its terms.


## Examples

The [documentation][] site has examples that are from the Subplot
source repository and typeset and tested by CI, so they're always up
to date. This means the examples always work.

[documentation]: https://doc.subplot.liw.fi/

Below is a pretty minimal example.

~~~~~{.markdown .numberLines}
---
title: "**echo**(1) acceptance tests"
template: python
bindings: echo.yaml
functions: echo.py
...

Introduction
=============================================================================

**echo**(1) is a Unix command line tool, which writes its command line
arguments to the standard output. This is a simple acceptance test
suite for the `/bin/echo` implementation.

No arguments
=============================================================================

```scenario
when user runs echo without arguments
then exit code is 0
then standard output contains a newline
then standard error is empty
```

Hello, world
=============================================================================

```scenario
when user runs echo with arguments hello, world
then exit code is 0
then standard output contains "hello, world"
then standard error is empty
```
~~~~~

See actual, working, tested examples:

* The Unix echo command
  ([html](https://doc.subplot.liw.fi/echo.html),
  [PDF](https://doc.subplot.liw.fi/echo.pdf),
  [source](https://gitlab.com/subplot/subplot/-/blob/main/examples/echo/echo.md)).

* A hypothetical JSON store with access control and a web API
  ([html](https://doc.subplot.liw.fi/muck.html),
  [PDF](https://doc.subplot.liw.fi/muck.pdf),
  [source](https://gitlab.com/subplot/subplot/-/blob/main/examples/muck/muck.md)).

* Subplot's own acceptance criteria, a non-trivial example
  ([html](https://doc.subplot.liw.fi/subplot.html),
  [PDF](https://doc.subplot.liw.fi/subplot.pdf),
  [source](https://gitlab.com/subplot/subplot/-/blob/main/subplot.md)).

# Architecture


```dot
digraph "architecture" {
md [label="foo.md \n (document, Markdown)"];
md [shape=box];

bindings [label="foo.yaml \n (bindings, YAML)"];
bindings [shape=box];

impl [label="foo.py \n (step implemenations, Python)"]
impl [shape=box];

subplot [label="Subplot"];
subplot [shape=ellipse];

pdf [label="foo.pdf \n PDF (generated)"]
pdf [shape=note];

html [label="foo.html \n HTML (generated)"]
html [shape=note];

testprog [label="test.py \n test program\n(generated)"]
testprog [shape=note];

report [label="Test report \n (stdout of test.py)"]
report [shape=note];

md -> subplot;
bindings -> subplot;
impl -> subplot;
subplot -> pdf;
subplot -> html;
subplot -> testprog;
testprog -> report;
}
```


# A bonus kitten

Because you scrolled all the way down.

[[!img kitten.jpg class=float_right
   alt="Kitten being adorable. By Ckahler on English Wikipedia"]]
