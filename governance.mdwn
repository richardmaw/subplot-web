[[!meta title="Governance"]]

The Subplot project is currently a hobby project between Lars and
Daniel, who decide everything by consensus.

All changes to the git repositories require the other to review and
merge to master.

A fuller governance model will be developed when the project starts
growing other contributors.
